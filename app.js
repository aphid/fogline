const fs = require("fs");
const pipeline = require("stream").pipeline;
const promisify = require("util").promisify;
const fetch = require("cross-fetch");

const {
    DateTime
} = require("luxon");

const {
    Interval
} = require("luxon");

let settings = JSON.parse(fs.readFileSync("settings.json"));

let dt = DateTime.now().setZone("America/Los_Angeles");

let lists = [];

const streamPipeline = promisify(pipeline);



let Listen = function(name, id, start, end, options) {
    this.first = true;
    this.nextDay = false;
    this.recording = false;
    this.controller = new AbortController();
    this.stored = {
        id: id,
        start: start,
        end: end,
        options: options
    };
    this.name = name;
    this.id = id;

    let temptime = start.split(":");
    this.start = DateTime.now().setZone("America/Los_Angeles").set({
        hour: temptime[0],
        minute: temptime[1]
    });

    temptime = end.split(":");
    this.end = DateTime.now().setZone("America/Los_Angeles").set({
        hour: temptime[0],
        minute: temptime[1]
    });

    let dt = DateTime.now().setZone("America/Los_Angeles");
    if (this.end.hour < this.start.hour) {
        this.nextDay = true;
    }

    if (this.nextDay) {

        console.log("++++++++", this.end.day)

        this.end = this.end.set({
            day: (this.end.day + 1)
        });
        console.log("++++++++", this.end.day)

    }

    if (this.start.hour < dt.hour) {
        console.log(this.name, "moving to tomorrow")
        this.end = this.end.set({
            day: (this.end.day + 1)
        });
        this.start = this.start.set({
            day: (this.start.day + 1)
        });
    }

    console.log(this.name, "next start is ", this.start.toString());
    console.log(this.name, "next end is ", this.end.toString());


    if (options) {
        for (let o in options) {
            this[o] = options[o];
        }
    }
    this.heartbeat();
};

Listen.prototype.checkTime = async function() {
    let dt = DateTime.now().setZone("America/Los_Angeles");
    let i = Interval.fromDateTimes(this.start, this.end);
    if (i.contains(dt)) {
        if (this.first) {
            this.first = false;

            console.log(dt.toLocaleString(DateTime.DATETIME_MED), "is between", this.start.toLocaleString(DateTime.DATETIME_MED), "and", this.end.toLocaleString(DateTime.DATETIME_MED));
        }
        return true;
    }
    if (this.first) {
        this.first = false;

        console.log(dt.toLocaleString(DateTime.DATETIME_MED), "is NOT between", this.start.toLocaleString(DateTime.DATETIME_MED), "and", this.end.toLocaleString(DateTime.DATETIME_MED));
    }
    return false;
};

Listen.prototype.record = async function() {
    let list = this;
    this.recording = true;
    let dt = DateTime.now().setZone("America/Los_Angeles");
    let i = Interval.fromDateTimes(dt, this.end);
    let howlong = i.length("milliseconds");
    let fn = "" + dt.toISODate() + "_" + dt.toISOTime().split("-")[0].split(".")[0];
    targetFile = `./${this.name}_${fn}.mp3`;
    console.log(targetFile);
    console.log(this.name, "recording for", i.length("seconds"));
    let authurl;
    try {
        authurl = `http://${settings.user}:${settings.password}@audio.broadcastify.com/${this.id}.mp3`;
        console.log("trying", authurl);
        const response = await fetch(authurl, {
            signal: this.controller.signal
        });
        if (!response.ok) throw new Error(`unexpected response ${response.statusText}`);
        await streamPipeline(response.body, fs.createWriteStream(targetFile));

    } catch (e) {
        console.log(authurl);
        console.log("caught", e);
        this.recording = false;
    }

};

Listen.prototype.stop = function() {
    this.recording = false;
    console.log("stopping", this.name);
    //console.log(this.controller);
    try {
        this.controller.abort("stop");
    } catch (e) {
        console.log(e);
    }
    this.start = this.start.set({
        day: this.start.day + 1
    });
    this.end = this.end.set({
        day: this.end.day + 1
    });

}

Listen.prototype.heartbeat = async function() {
    await sleep(settings.heartbeat);
    let ct = await this.checkTime();
    if (!this.recording && ct) {
        console.log("not recording and should be")
        this.record();
    } else if (ct && this.recording) {
        //console.log(this.name + " is recording.");
    } else if (!ct && this.recording) {
        console.log(this.name, " should stop recording");
        this.stop();
    } else {
        //console.log("waiting");
        //console.log(this.controller);

    }
    await this.heartbeat();
};

//http://USERNAME:PASSWORD@audio.broadcastify.com/36119.mp3
let bart = new Listen("BART", 2776, "23:00", "05:00");
let harbor = new Listen("Harbor", 36119, "04:00", "10:00");
//let test = new Listen("test", 36119, "04:00", "24:00");


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}